import { motion, AnimatePresence } from "framer-motion";
import { ICartConfirmProps } from "../interfaces/interfaces";
import productsInCart from "../src/store/productsInCart";

const CartConfirmClearBtn: React.FC<ICartConfirmProps> = (props) => {
    const confirmClearBtnHandleClick = (): void => {
        productsInCart.clearCart();
    };

    return (
        <AnimatePresence>
            { props.active && 
            <motion.button
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
                whileHover={{ scale: 1.01 }}
                transition={{ duration: 0.2 }}
                type="button"
                data-role="confirmClear"
                onClick={confirmClearBtnHandleClick}>
                    <span>clear cart?</span>
            </motion.button> 
            }
        </AnimatePresence>
    );
};

export default CartConfirmClearBtn;