import { motion, AnimatePresence } from "framer-motion";
import { ISpinnerProps } from "../interfaces/interfaces";

const LoadingSpinner: React.FC<ISpinnerProps> = (props) => {
    return (
        <AnimatePresence>
            {props.active && 
                <motion.div 
                    className="spinner spinner--loading width-100" 
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    exit={{ opacity: 0 }}
                    transition={{ duration: 0.3 }}>
                </motion.div>
            }
        </AnimatePresence>
    );
};

export default LoadingSpinner;