import { useEffect, useState } from "react";
import { motion, AnimatePresence } from "framer-motion";
import { ICartPopUpProps } from "../interfaces/interfaces";

const CartPopUp: React.FC<ICartPopUpProps> = (props) => {
    const [visible, setVisible] = useState<boolean>(false);

    useEffect(() => {
        setVisible(props.active);
    }, [props.active])

    const list = {
        visible: {
            x: 0,
            transition: {
                type: 'spring',
                duration: 0.3,
                bounce: 0.4
            },
        },
        hidden: {
            x: 150,
        },
    }

    return (
        <AnimatePresence>
            {visible &&
                <motion.div
                    initial="hidden"
                    animate="visible"
                    exit="hidden"
                    variants={list}
                    className="header__cartContainer__popup">
                        <span>Item succesfully added!</span>
                        <a>go to cart</a>
                </motion.div>
            }
        </AnimatePresence>
    );
};

export default CartPopUp;