import { useState } from 'react';
import classNames from 'classnames';
import { Field, Form, Formik } from 'formik';
import * as yup from 'yup';
import { IFormValues, IUserInfoFormProps } from '../interfaces/interfaces';

const UserInfoForm: React.FC<IUserInfoFormProps> = (props) => {
    const [clicked, setClicked] = useState<boolean>(false);
    const deliveryDescrClasses: string = classNames('form__delivery__container__deliveryDescr', { hidden: !clicked });

    const phoneRegExp: RegExp = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;

    const validateTextArea = (): string | void => {
        if (clicked) {
            return 'Required';
        }
    }

    const UserInfoSchema = yup.object().shape({
        firstName: yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required')
            .matches(/^[a-zA-Z.\-_$@*!]{2,50}$/, 'First name should be in correct format'),
        lastName: yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required')
            .matches(/^[a-zA-Z.\-_$@*!]{2,50}$/, 'Second name should be correct format'),
        email: yup.string()
            .email('Invalid email')
            .required('Required'),
        phoneNumber: yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required')
            .matches(phoneRegExp, 'Phone number is not valid'),
        delivery: yup.string()
            .required('Required'),
        deliveryInfo: yup.string()
            .required(validateTextArea)
            .min(5, 'Too Short!')
    });

    const radioDeliveryHandleClick = (value: boolean): void => {
        setClicked(value);
    };

    return (
        <div className="main__productCart__formContainer">
            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    email: '',
                    phoneNumber: '',
                    delivery: '',
                    deliveryInfo: '',
                    totalSum: +props.totalSum.toFixed(2),
                }}
                validationSchema={UserInfoSchema}
                onSubmit={( values: IFormValues ) => {
                    if (!clicked) {
                        values.deliveryInfo = '';
                    }

                    values.totalSum = +props.totalSum.toFixed(2);
                    console.log(values)
                }}
            >
            {({ errors, touched }) => (
                <Form className="main__productCart__formContainer__form form">
                    <h3>Personal info</h3>
                    <label htmlFor="firstName">First Name</label>
                    <Field id="firstName" name="firstName" placeholder="John" />
                    {errors.firstName && touched.firstName ? (
                        <div className="form__errors">{errors.firstName}</div>
                    ) : null}

                    <label htmlFor="lastName">Last Name</label>
                    <Field id="lastName" name="lastName" placeholder="Doe" />
                    {errors.lastName && touched.lastName ? (
                        <div className="form__errors">{errors.lastName}</div>
                    ) : null}

                    <label htmlFor="email">Email</label>
                    <Field
                        id="email"
                        name="email"
                        placeholder="johndoe@mail.com"
                        type="email"
                    />
                    {errors.email && touched.email ? <div className="form__errors">{errors.email}</div> : null}
                    
                    <label htmlFor="phoneNumber">Phone number</label>
                    <Field id="phoneNumber" name="phoneNumber" type="tel" placeholder="+XX XXX XX XX XXX"/>
                    {errors.phoneNumber && touched.phoneNumber ? <div className="form__errors lastInContainer">{errors.phoneNumber}</div> : null}

                    <h3>Delivery info</h3>
                    <div className="form__delivery">
                        <h4>Please, choose the delivery options:</h4>
                        <div className="form__delivery__container">
                            <div>
                                <label htmlFor="personalPickup">personal pickup</label>
                                <Field type="radio" id="personalPickup" name="delivery" value="personal" onInput={() => radioDeliveryHandleClick(false)} />
                            </div>
                            <div>
                                <label htmlFor="deliverByPost">deliver by post</label>
                                <Field type="radio" id="deliverByPost" name="delivery" value="byPost" onInput={() => radioDeliveryHandleClick(true)} />
                            </div>
                            
                            <Field className={deliveryDescrClasses} name="deliveryInfo" as="textarea" placeholder="Enter delivery address, please"/>
                            {clicked && errors.deliveryInfo && touched.deliveryInfo ? (
                                <div className="form__errors textArea">{errors.deliveryInfo}</div>
                            ) : null}
                        </div>
                        {errors.delivery && touched.delivery ? (
                            <div className="form__errors">{errors.delivery}</div>
                        ) : null}
                    </div>

                    <button type="submit">Submit</button>
                </Form>
            )}
            </Formik>
        </div>
    )
};

export default UserInfoForm;