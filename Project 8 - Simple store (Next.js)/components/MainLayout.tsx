import { useEffect, useState } from "react";
import Link from "next/link";
import Image from 'next/image';
import { useRouter } from "next/router";
import { observer } from "mobx-react-lite";
import classNames from "classnames";
import CartPopUp from "./CartPopUp";
import LoadingSpinner from "./LoadingSpinner";
import { ProductsInCart } from "../types/types";
import productsInCart from "../src/store/productsInCart";
import activePopup from "../src/store/activePopup";
import cartLogo from '../public/svg/shopping-cart-outline-svgrepo-com.svg';

const MainLayout: React.FC = observer((props) => {
    const logoClasses: string = classNames('header__cartContainer__indicator', { hidden: productsInCart.productsInCart.length === 0 });
    const router = useRouter();
    const [spinnerActive, setSpinnerActive] = useState<boolean>(false);

    useEffect(() => {
        if (productsInCart.isFirstLoad) {
            window.addEventListener("beforeunload", () => {
                const dataInStore: ProductsInCart = productsInCart.productsInCart;
                localStorage.setItem("cart-items", JSON.stringify(dataInStore));
            });
    
            productsInCart.getDataOnload();
        }

        return (() => {
            clearTimeout(activePopup.timer!);
        })
    }, [])

    const productHandleClick = (): void => {
        if (router.pathname.endsWith('catalog')) {
            return;
        }

        setSpinnerActive(true);
    };

    const activePopupMouseOver = (): void => {
        activePopup.mouseOverPopup();
    };

    const activePopupMouseLeave = (): void => {
        activePopup.mouseLeavePopup();
    };

    return (
        <>
            <header className="header">
                <Link href={'/'}>
                    <img className="header__logo" src="https://www.logoinspirations.co/wp-content/uploads/2018/11/hand-logo-22.jpg" alt="logo"/>
                </Link>
                <nav>
                    <Link href={'/'}><a>home</a></Link>
                    <Link href={'/about'}><a>about</a></Link>
                    <Link href={'/catalog'}>
                        <a onClick={productHandleClick}>
                            {spinnerActive ?
                                <LoadingSpinner active={spinnerActive}/> :
                                <span>catalog</span>
                            }
                        </a>
                    </Link>
                </nav>
                <Link href={'/productCart'}>
                    <div className="header__cartContainer" onMouseOver={activePopupMouseOver} onMouseLeave={activePopupMouseLeave}>
                        <Image alt="Cart logo" src={cartLogo} width={35} height={35} />
                        <div className={logoClasses}>{productsInCart.productsInCart.length}</div>
                        <CartPopUp active={activePopup.activePopup} />
                    </div>
                </Link>
            </header>
            <main className="main">
                {props.children}
            </main>
        </>
    )
});

export default MainLayout;