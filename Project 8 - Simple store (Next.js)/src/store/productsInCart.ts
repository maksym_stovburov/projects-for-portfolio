import { makeAutoObservable } from "mobx";
import { IDataProduct } from "../../interfaces/interfaces";
import { AllData, ProductsInCart, UniquieProducts } from "../../types/types";

class ProductCart {
    productsInCart: ProductsInCart = [];
    isFirstLoad: boolean = true;

    constructor() {
        makeAutoObservable(this);
    }

    addNewElem(elem: string) {
        this.productsInCart.push(elem);
    }

    decreaseElem(elemId: number) {
        const elemIndex: number = this.productsInCart.findIndex(( item: string ) => JSON.parse(item).id === elemId);
        this.productsInCart.splice(elemIndex, 1);
    }

    clearCart() {
        this.productsInCart.length = 0;
    }

    removeElem(elemId: number) {
        this.productsInCart = this.productsInCart.filter((item: string) => JSON.parse(item).id !== elemId);
    }

    getCartStructure(): Array<UniquieProducts> {
        const refactoredStore: AllData = this.productsInCart.map(( item: string ) => JSON.parse(item));
        refactoredStore.sort(( a: IDataProduct, b: IDataProduct ) => a.id - b.id);
        const setOfProducts = new Set(refactoredStore.map(( item: IDataProduct ) => item.id));
        const elemsInCart: Array<UniquieProducts> = []; ////[object, quantity]

        setOfProducts.forEach(( value ) => {
            const quantinty: AllData = refactoredStore.filter(( item: IDataProduct ) => item.id === value);
            elemsInCart.push([refactoredStore.find(( item: IDataProduct ) => item.id === value )!, quantinty.length]);
        });

        return elemsInCart;
    };

    getSingleProductCount(productId: number): number {
        const cartStructure: Array<UniquieProducts> = this.getCartStructure();
        const desiredElem: UniquieProducts = cartStructure.find(( item: UniquieProducts ) => item[0].id === productId)!;

        return desiredElem ? desiredElem[1] : 0;
    }

    getDataOnload() {
        this.isFirstLoad = false;
        
        if (!localStorage.getItem("cart-items")) {
            return;
        }

        const data: ProductsInCart = JSON.parse(localStorage.getItem("cart-items")!);
        this.productsInCart.push(...data);
    }
}

export default new ProductCart();