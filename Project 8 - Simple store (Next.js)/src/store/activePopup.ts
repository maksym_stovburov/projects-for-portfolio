import { makeAutoObservable } from "mobx";
import { configure } from "mobx"

configure({
    enforceActions: "never",
})

class ActivePopup {
    activePopup: boolean = false;
    timer: ReturnType<typeof setTimeout> | null = null;

    constructor() {
        makeAutoObservable(this);
    }

    setPopupStateActive() {
        if (this.activePopup) {
            clearTimeout(this.timer!);
        }

        this.activePopup = true;

        this.timer = setTimeout(() => {
            this.activePopup = false;
        }, 4000);
    };

    setPopupStatePassive() {
        this.activePopup = false;
    }

    mouseOverPopup() {
        clearTimeout(this.timer!);
    }

    mouseLeavePopup() {
        this.setPopupStatePassive();
    }
}

export default new ActivePopup();