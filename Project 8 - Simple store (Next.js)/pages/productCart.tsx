import { MutableRefObject, useEffect, useRef, useState } from 'react';
import type { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { observer } from 'mobx-react-lite';
import { ClearOutlined } from '@mui/icons-material';
import MainLayout from '../components/MainLayout';
import UserInfoForm from '../components/UserInfoForm';
import CartConfirmClearBtn from '../components/CartConfirmClearBtn';
import productsInCart from '../src/store/productsInCart';
import { IDataProduct } from '../interfaces/interfaces';
import { UniquieProducts } from '../types/types';

const ProductCart: NextPage = observer(() => {
    let totalSum: number = 0;
    const refsArr: MutableRefObject<HTMLLIElement[]> = useRef(new Array());
    const [active, setActive] = useState<boolean>(false);

    useEffect(() => {
        return (() => {
            refsArr.current.length = 0;
        })
    }, [])

    const addBtnHandleClick = (elem: IDataProduct): void => {
        productsInCart.addNewElem(JSON.stringify(elem));
    };

    const searchRefElem = (elemId: number): HTMLLIElement => {
        const result = refsArr.current.find((item: HTMLLIElement) => Number(item.getAttribute('data-id')) === elemId);

        return result!;
    };

    const removeBtnHandleClick = (elemId: number, quantity: number): void => {
        if (quantity === 1) {
            const parentElem = searchRefElem(elemId);
            parentElem.classList.add('showConfirm');
        } else {
            productsInCart.decreaseElem(elemId);
        }
    };

    const acceptBtnHandleClick = (elemId: number): void => {
        productsInCart.removeElem(elemId);
    };

    const declineBtnHandleClick = (elemId: number): void => {
        const parentElem = searchRefElem(elemId);

        parentElem.classList.remove('showConfirm');
    };

    const clearBtnHandleClick = (): void => {
        setActive(true);
    };

    const clearBtnMouseLeave = (): void => {
        setActive(false);
    };

    const getTotalSum = (elems: Array<UniquieProducts>): number => {
        const result = elems.reduce((acc: number, item: UniquieProducts) => acc += item[0].price*item[1], 0);

        return result;
    };

    const productHandleClick = (elemId: number): void => {
        const parentElem = searchRefElem(elemId);

        parentElem.classList.add('spinner--loading');
    };

    const productHandleMouseLeave = (e: React.MouseEvent): void => {
        if (e.currentTarget.classList.contains('showConfirm')) {
            e.currentTarget.classList.remove('showConfirm');
        }
    };

    const deleteBtnHandleClick = (elemId: number): void => {
        const parentElem = searchRefElem(elemId);

        parentElem.classList.add('showConfirm');
    };

    const returnProductsInCart = (): JSX.Element => {
        const cartStructure = productsInCart.getCartStructure() as Array<UniquieProducts>;
        totalSum = getTotalSum(cartStructure);
        refsArr.current.length = 0;

        return (
            <ul className="main__productCart__productsInCart productsInCart">
                {cartStructure.map(( item: UniquieProducts ) => (
                    <li className="productsInCart__eachProduct spinner" data-id={item[0].id} key={item[0].id} ref={(element: HTMLLIElement) => element ? refsArr.current.push(element) : null} onMouseLeave={productHandleMouseLeave}>
                        <Link href={'/catalog/[category]/[productId]'} as={`/catalog/${item[0].category}/${item[0].id}`}>
                            <div className="productsInCart__eachProduct__imageContainer" onClick={() => productHandleClick(item[0].id)}>
                                <img data-role="linkFromCart" src={item[0].image} alt="product"/>
                            </div>
                        </Link>
                        <div className="productsInCart__eachProduct__infoContainer">
                            <Link href={'/catalog/[category]/[productId]'} as={`/catalog/${item[0].category}/${item[0].id}`}>
                                <h3 data-role="linkFromCart" onClick={() => productHandleClick(item[0].id)}>{item[0].title}</h3>
                            </Link>
                            <span>{item[0].price.toFixed(2)} $</span>
                            <div className="productsInCart__eachProduct__infoContainer__buttons">
                                <button type="button" onClick={() => removeBtnHandleClick(item[0].id, item[1])}>-</button>
                                <span>{item[1]}</span>
                                <button type="button" onClick={() => addBtnHandleClick(item[0])}>+</button>
                            </div>
                        </div>
                        <div className="productsInCart__eachProduct__confirmContainer">
                            <p>Remove product from cart?</p>
                            <div className="productsInCart__eachProduct__confirmContainer__buttons">
                                <button type="button" data-role="accept" onClick={() => acceptBtnHandleClick(item[0].id)}>
                                    <span>yes</span>
                                </button>
                                <button type="button" onClick={() => declineBtnHandleClick(item[0].id)}>
                                    <span>no</span>
                                </button>
                            </div>
                        </div>
                        <button type="button" data-role="deleteAllBtn" className="productsInCart__eachProduct__removeCrossBtn" onClick={() => deleteBtnHandleClick(item[0].id)}>
                            <ClearOutlined color="error" data-role="deleteAllBtn"/>
                        </button>
                    </li>
                ))}
                <p className="productsInCart__totalSum">Total sum: <span>{totalSum.toFixed(2)} $</span></p>
            </ul>
        );
    };
    
    return (
        <MainLayout>
            <Head>
                <title>Next App | Cart page</title>
            </Head>
            <div className="main__productCart">
                {productsInCart.productsInCart.length === 0 ? 
                    <h2 className="productsInCart--empty">The cart is empty</h2>
                    : <>
                        {returnProductsInCart()}
                        <div className="main__productCart__clearCartContainer" onMouseLeave={clearBtnMouseLeave}>
                            <CartConfirmClearBtn active={active} />
                            <button data-role="clearBtn" type="button" onClick={clearBtnHandleClick}>
                                <span>remove all</span>
                            </button>
                        </div>
                        <UserInfoForm totalSum={totalSum} />
                    </>
                }
            </div>
        </MainLayout>
    )
});

export default ProductCart;