import { useEffect, useRef, useState } from 'react';
import type { GetServerSidePropsContext, NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { observer } from 'mobx-react-lite';
import MainLayout from '../../components/MainLayout';
import LoadingSpinner from '../../components/LoadingSpinner';
import productsInCart from '../../src/store/productsInCart'
import activePopup from '../../src/store/activePopup';
import { IDataProduct, IProductsProps } from '../../interfaces/interfaces';
import { AllData } from '../../types/types';

const Products: NextPage<IProductsProps> = observer(({ data }) => {
    const router = useRouter();
    const refsArr = useRef(new Array());
    const [spinnerActive, setSpinnerActive] = useState<boolean>(false);

    useEffect(() => {
        return () => {
            activePopup.setPopupStatePassive();
            refsArr.current.length = 0;
        }
    }, []);

    const backBtnHandleClick = (): void => {
        setSpinnerActive(true);
        router.push('/catalog');
    };

    const addBtnHandleClick = (elem: IDataProduct): void => {
        productsInCart.addNewElem(JSON.stringify(elem));
        activePopup.setPopupStateActive();
    };

    const productHandleClick = (elemId: number): void => {
        const btnElem = refsArr.current.find((item: HTMLDataListElement) => Number(item.getAttribute('data-id')) === elemId);
        btnElem.classList.add('spinner--loading');
    };
    
    const returnProducts = (products: AllData): JSX.Element => {
        return (
            <ul className="main__productListContainer__productList productList">
                {products.map((item: IDataProduct) => (
                    <li className="productList__singleProduct" key={item.id}>
                        <Link href={'/catalog/[category]/[productId]'} as={`/catalog/${router.query.category}/${item.id}`}>
                            <div className="productList__singleProduct__linkWrapper" onClick={() => productHandleClick(item.id)}>
                                <div className="productList__singleProduct__linkWrapper__imageContainer">
                                    <img src={item.image} alt="item"/>
                                </div>
                                <div className="productList__singleProduct__linkWrapper__titleContainer" >
                                    <h3>{item.title}</h3>
                                </div>
                            </div>
                        </Link>
                        <span className="productList__singleProduct__price">{item.price.toFixed(2)} $</span>
                        <div className="productList__singleProduct__buttonsContainer">
                            <button type="button" onClick={() => addBtnHandleClick(item)}>
                                add to cart
                            </button>
                            <Link href={'/catalog/[category]/[productId]'} as={`/catalog/${router.query.category}/${item.id}`}>
                                <button type="button" className="spinner" data-id={item.id} ref={(element) => element ? refsArr.current.push(element) : null} onClick={() => productHandleClick(item.id)}>
                                    <span className="spinner__text">see info</span>
                                </button>
                            </Link>
                        </div>
                    </li>
                ))}
            </ul>
        );
    };

    return (
        <MainLayout>
            <Head>
                <title>Next App | Products page</title>
            </Head>
            <div className="main__productListContainer">
                <button type="button" className="main__productListContainer__goBackBtn goBackBtn" onClick={backBtnHandleClick}>
                    {spinnerActive ?
                        <LoadingSpinner active={spinnerActive}/> :
                        <span>go back</span>
                    }
                </button>
                <p>Category: {router.query.category}</p>
                {data && returnProducts(data)}
            </div>
        </MainLayout>
    )
});

export async function getServerSideProps(context: GetServerSidePropsContext) {
    const res: Response = await fetch(`https://fakestoreapi.com/products/category/${context.params?.category}`);

    if (!res.ok) {
        return {
            notFound: true
        };
    }

    const data: JSON = await res.json();

    return { props: { data } };
}

export default Products;