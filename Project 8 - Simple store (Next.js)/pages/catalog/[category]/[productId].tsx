import { useEffect, useState } from 'react';
import type { GetServerSidePropsContext, NextPage } from 'next';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { observer } from 'mobx-react-lite';
import classNames from 'classnames';
import MainLayout from '../../../components/MainLayout';
import LoadingSpinner from '../../../components/LoadingSpinner';
import productsInCart from '../../../src/store/productsInCart';
import activePopup from '../../../src/store/activePopup';
import { ISingleProductProps, IDataProduct } from '../../../interfaces/interfaces';

const ProductPage: NextPage<ISingleProductProps> = observer(({ data }) => {
    const router = useRouter();
    const product: IDataProduct = data;
    const productsInCartCount: number = productsInCart.getSingleProductCount(product.id) as number;
    const disableBtnClasses: string = classNames({ disabled: productsInCartCount === 0 });
    const [spinnerActive, setSpinnerActive] = useState<boolean>(false);

    useEffect(() => {
        return () => {
            activePopup.setPopupStatePassive();
        }
    }, [])

    const backBtnHandleClick = (): void => {
        setSpinnerActive(true);
        router.push(`/catalog/${router.query.category}`);
    };

    const addBtnHandleClick = (elem: IDataProduct): void => {
        productsInCart.addNewElem(JSON.stringify(elem));
        activePopup.setPopupStateActive();
    };

    const removeBtnHandleClick = (elemId: number): void => {
        if (productsInCartCount === 0) {
            return;
        }

        productsInCart.decreaseElem(elemId);
    };

    return (
        <MainLayout>
            <Head>
                <title>Next App | Single product page</title>
            </Head>
            <div className="main__productPage">
                <button type="button" className="main__productPage__goBackBtn goBackBtn spinner" onClick={backBtnHandleClick}>
                {spinnerActive ?
                        <LoadingSpinner active={spinnerActive}/> :
                        <span>to categories</span>
                    }
                </button>
                <div className="main__productPage__productPageContent productPageContent">
                    <div className="productPageContent__imageContainer">
                        <img src={product.image} alt="product"/>
                    </div>
                    <div className="productPageContent__infoContainer infoContainer">
                        <h2>{product.title}</h2>
                        <p>{product.description}</p>
                        <span>{product.price.toFixed(2)} $</span>
                        <div className="infoContainer__buttons">
                            <button type="button" className={disableBtnClasses} onClick={() => removeBtnHandleClick(product.id)}>-</button>
                            <span>{productsInCartCount}</span>
                            <button type="button" onClick={() => addBtnHandleClick(product)}>+</button>
                        </div>
                    </div>
                </div>
            </div>
        </MainLayout>
    )
});

export async function getServerSideProps(context: GetServerSidePropsContext) {
    const res = await fetch(`https://fakestoreapi.com/products/${context.params?.productId}`);

    if (!res.ok) {
        return {
            notFound: true
        };
    }

    const data = await res.json();

    return { props: { data } };
}

export default ProductPage;