import type { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import MainLayout from '../../components/MainLayout';
import { ICatalogIndexProps } from '../../interfaces/interfaces';

const Catalog: NextPage<ICatalogIndexProps> = ({ data }) => {
    const linkHandleClick = (e: React.MouseEvent): void => {
        e.currentTarget.classList.add('spinner--loading');
    };

    return (
        <MainLayout>
            <Head>
                <title>Next App | Catalog page</title>
            </Head>
            <div className="main__categoriesContainer">
                <ul className="main__categoriesContainer__list">
                    {data.map((item: string) => (
                        <li key={item}>
                            <Link href={'/catalog/[category]'} as={`/catalog/${item}`}>
                                <a className="spinner" onClick={linkHandleClick}>
                                    <span className="spinner__text">{item}</span>
                                </a>
                            </Link>
                        </li>
                    ))}
                </ul>
            </div>
        </MainLayout>
    )
}

export async function getStaticProps() {
    const res = await fetch('https://fakestoreapi.com/products/categories');

    if (!res.ok) {
        throw new Error(`Failed to fetch categories, received status ${res.status}`)
    }

    const data = await res.json();

    return { 
        props: {
                data 
            },
        revalidate: 86400 //revalidates once per day
    };
}

export default Catalog;