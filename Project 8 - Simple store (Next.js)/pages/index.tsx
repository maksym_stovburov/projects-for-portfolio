import { useState } from 'react';
import type { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import LoadingSpinner from '../components/LoadingSpinner';
import MainLayout from '../components/MainLayout';

const Index: NextPage = () => {
  const [spinnerActive, setSpinnerActive] = useState<boolean>(false);

  const linkHandleClick = (): void => {
    setSpinnerActive(true);
  };

  return (
    <MainLayout>
      <Head>
        <title>Next App | Home page</title>
      </Head>
      <div className="main__indexPage">
        <h2>Welcome to the home page</h2>
        <h3>Choose the link:</h3>
        <div className="main__indexPage__links">
          <Link href={'/about'}>
            <a>about</a>
          </Link>
          <Link href={'/catalog'}>
            <a className="spinner" onClick={linkHandleClick}>
              {spinnerActive ?
                <LoadingSpinner active={spinnerActive}/> :
                <span>catalog</span>
              }
            </a>
          </Link>
        </div>
      </div>
    </MainLayout>
  )
}

export default Index;
