import type { NextPage } from 'next';
import Link from 'next/link';

const ErrorPage: NextPage = () => {
    return (
        <section className="errorPage">
            <h2>Oops... Something went wrong</h2>
            <h4>The page doesn't exist</h4>
            <h4>Please, return to the home page</h4>
            <Link href={'/'}>
                <a>return</a>
            </Link>
        </section>
    )
}

export default ErrorPage;