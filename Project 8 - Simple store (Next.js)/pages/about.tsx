import type { NextPage } from 'next';
import Head from 'next/head';
import MainLayout from '../components/MainLayout';

const About: NextPage = () => {
    return (
        <MainLayout>
            <Head>
                <title>Next App | About page</title>
            </Head>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas quas consectetur enim magnam facere fugit, aspernatur corrupti animi beatae vitae velit vero sunt laudantium? Debitis libero atque consequuntur delectus velit.</p>
        </MainLayout>
    )
}

export default About;