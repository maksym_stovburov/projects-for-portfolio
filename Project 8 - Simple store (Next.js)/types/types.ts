import { IDataProduct } from "../interfaces/interfaces";

type AllData = Array<IDataProduct>;
type ProductsInCart = Array<string>;
type UniquieProducts = [IDataProduct, number];

export type { AllData, ProductsInCart, UniquieProducts };