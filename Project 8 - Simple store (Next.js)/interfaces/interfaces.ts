import { AllData } from "../types/types";

interface IDataProduct {
    category: string;
    description?: string;
    id: number;
    image?: string;
    price: number;
    rating?: {
        count: number;
        rate: number;
    }
    title: string;
}

interface ISingleProductProps {
    data: IDataProduct;
}

interface IProductsProps {
    data: AllData;
}

interface ICatalogIndexProps {
    data: string[];
}

interface IUserInfoFormProps {
    totalSum: number;
}

interface ICartPopUpProps {
    active: boolean;
}

interface ICartConfirmProps extends ICartPopUpProps {
}

interface ISpinnerProps extends ICartPopUpProps {
}

interface IFormValues {
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    delivery: string;
    deliveryInfo: string;
    totalSum: number;
}

export type { IDataProduct, ISingleProductProps, IProductsProps, ICatalogIndexProps, IUserInfoFormProps, ICartPopUpProps, ICartConfirmProps, ISpinnerProps, IFormValues };