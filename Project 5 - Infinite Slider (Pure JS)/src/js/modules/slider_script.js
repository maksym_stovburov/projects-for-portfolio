"use strict";

window.addEventListener('load', () => {
    class Slider {

        constructor() {
            this.largeULs = document.querySelectorAll('.slider__largeSlides');
            this.largeLIs = this.largeULs[0].querySelectorAll('.slider__largeSlides > li');
            this.img = this.largeULs[0].querySelector('.slider__largeSlides > li > img');
            this.activeElems = [];
            this.elemWidth = Number(parseInt(getComputedStyle(this.largeLIs[0]).width));
            this.slider = document.getElementById('slider');
            this.bigRes = window.matchMedia('(min-width: 1024px)').matches;
            this.pos = 0;
            this.stopWorking = false;
            this.left = false;
            this.minorULs = null;
            this.minorLIs = null;
            this.timer = null;
            this.previousValue = null;
            this.modalWindow = null;
            this.modalWindowContainer = null;
        }

        createMinorElems() {
            let minorUl = this.largeULs[0].cloneNode(true);
            minorUl.className = 'slider__minorSlides';
            minorUl.children[0].className = 'active-minor';
            this.slider.append(minorUl);
            this.minorULs = document.querySelectorAll('.slider__minorSlides');
            this.minorLIs = document.querySelectorAll('.slider__minorSlides > li');
        }

        preSets() {
            this.slider.style.minHeight = getComputedStyle(this.largeLIs[0]).height;
            this.minorULs[0].style.left = `calc(50% - ${getComputedStyle(this.minorULs[0]).width}/2)`;
            this.minorULs[0].style.minWidth = `${getComputedStyle(this.minorULs[0]).width}`;

            for (let i = 0; i < this.largeLIs.length; i++) {
                this.largeLIs[i].children[0].setAttribute('data-count', `${i}`);
                this.minorLIs[i].children[0].setAttribute('data-count', `${i}`);
            }

            this.slider.insertAdjacentHTML('afterend', `
                <div class="modal hide">
                    <div class="modal__container"></div>
                </div>
            `);

            this.modalWindow = document.querySelector('.modal');
            this.modalWindowContainer = this.modalWindow.querySelector('.modal > div');
            
            if (this.bigRes) {
                for (let i = 0; i < 3; i++) {
                    this.largeLIs[i].classList.add('active');
                    this.minorLIs[i].classList.add('active-minor');
                }

                this.setPosition();
            } else {
                this.slider.style.maxWidth = getComputedStyle(this.img).width;
            }
        }

        shiftElems() {
            let context = this;

            context.timer = setTimeout(function shift() {
                if (context.stopWorking) {
                    return;
                } 
                
                context.shiftRight(false, context);

                context.timer = setTimeout(shift, 3550); /* 0.55 seconds for transition and 3 seconds for delay*/
            }, 3550);
        }
    
        zoomImage(e, context) {
            if (e.target.tagName === 'IMG') {
                if (context.stopWorking) {
                    return;
                }

                this.modalWindow.classList.remove('hide');
                this.modalWindowContainer.innerHTML = e.target.parentNode.innerHTML;

            } else {
                this.modalWindow.classList.add('hide');
                this.modalWindowContainer.innerHTML = '';
            }

            context.stopWorking = !context.stopWorking;
                
            if (!context.stopWorking) {
                clearTimeout(context.timer);
                context.shiftElems();
            }
        }

        shiftSmallRes(left) {
            this.largeLIs[this.pos].classList.remove('active');
            this.dotsActiveSlide(this.pos, left);

            if (left) {
                if (this.pos === 0) {
                    this.pos = this.largeLIs.length;
                }
                this.largeLIs[this.pos-1].classList.add('active');
            } else {
                if (this.pos === this.largeLIs.length - 1) {
                    this.pos = -1;
                }
                this.largeLIs[this.pos+1].classList.add('active');
            }
        }
    
        shiftLeft(context) {
            clearTimeout(context.timer);
            context.shiftElems();
        
            if (context.bigRes) {
                context.left = true;
            
                if (context.pos === context.largeLIs.length - 1) {
                    context.largeLIs[1].classList.remove('active');
                } else if (context.pos === context.largeLIs.length - 2) {
                    context.largeLIs[0].classList.remove('active');
                } else {
                    context.largeLIs[context.pos+2].classList.remove('active');
                }
        
                context.dotsActiveSlide(context.pos, true);
        
                if (context.pos === 0) {
                    context.pos = context.largeLIs.length;
                } 
        
                context.largeLIs[context.pos-1].classList.add('active');
                context.setPosition(context.pos-1, context.pos, context.pos+1, context.pos);
            } else {   
                context.shiftSmallRes(true);
            }
        
            context.pos--;
        }
    
        shiftRight(e, context) {
            if (e) {
                clearTimeout(context.timer);
                context.shiftElems();
            }

            if (context.bigRes) {
                context.left = false;
                context.largeLIs[context.pos].classList.remove('active');
                context.dotsActiveSlide(context.pos, false);
        
                if (context.pos === context.largeLIs.length - 1) {
                    context.pos = -1;
                } 
        
                if (context.pos === context.largeLIs.length - 3) {
                    context.largeLIs[0].classList.add('active');
                } else if (context.pos === context.largeLIs.length - 2) {
                    context.largeLIs[1].classList.add('active');
                } else {
                    context.largeLIs[context.pos+3].classList.add('active');
                }
                
                context.setPosition(context.pos+1, context.pos+2, context.pos+3, context.pos);
            } else {
                context.shiftSmallRes(false);
            }
        
            context.pos++;
        }
    
        dotsActiveSlide(pos, directionLeft, directTarget) {
            if (this.bigRes) {
                if (directTarget || directTarget === 0) {
                    this.minorLIs.forEach((item) => {
                        if (item.classList.contains('active-minor')) {
                            item.classList.remove('active-minor');
                        }
                    });
                    this.minorLIs[directTarget].classList.add('active-minor');
            
                    if (directTarget === this.largeLIs.length - 1) {
                        this.minorLIs[0].classList.add('active-minor');
                        this.minorLIs[1].classList.add('active-minor');
                    } else if (directTarget === this.largeLIs.length - 2) {
                        this.minorLIs[this.largeLIs.length - 1].classList.add('active-minor');
                        this.minorLIs[0].classList.add('active-minor');
                    } else {
                        this.minorLIs[directTarget+1].classList.add('active-minor');
                        this.minorLIs[directTarget+2].classList.add('active-minor');
                    }
            
                } else  {
            
                    if (directionLeft) {
                        if (pos === this.largeLIs.length - 2) {
                            this.minorLIs[0].classList.remove('active-minor');
                        } else if (pos === this.largeLIs.length - 1) {
                            this.minorLIs[1].classList.remove('active-minor');
                        } else {
                            this.minorLIs[pos+2].classList.remove('active-minor');
                        }
                
                        if (pos === 0) {
                            this.minorLIs[this.largeLIs.length-1].classList.add('active-minor');
                        } else {
                            this.minorLIs[pos-1].classList.add('active-minor');
                        }
                
                    } else {
                        this.minorLIs[pos].classList.remove('active-minor');
                
                        if (pos === this.largeLIs.length - 3) {
                            this.minorLIs[0].classList.add('active-minor');
                        } else if (pos === this.largeLIs.length - 2) {
                            this.minorLIs[1].classList.add('active-minor');
                        } else if (pos === this.largeLIs.length - 1) {
                            this.minorLIs[2].classList.add('active-minor');
                        } else {
                            this.minorLIs[pos+3].classList.add('active-minor');
                        }
                    }
                }
            } else {
                this.minorLIs[pos].classList.remove('active-minor');
    
                if (directTarget || directTarget === 0) {
                    this.minorLIs[directTarget].classList.add('active-minor');
                } else {
                    if (directionLeft) {
                        if (pos === 0) {
                            pos = this.largeLIs.length;
                        }
                
                        this.minorLIs[pos-1].classList.add('active-minor');
                    } else {
                        
                        if (pos === this.largeLIs.length - 1) {
                            pos = -1;
                        }
                
                        this.minorLIs[pos+1].classList.add('active-minor');
                    }
                }
            }
        }
    
        createArrows() {

            this.slider.insertAdjacentHTML('afterbegin', `
                <button class="arrowLeft arrows">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm2 12l-4.5 4.5 1.527 1.5 5.973-6-5.973-6-1.527 1.5 4.5 4.5z"/>
                    </svg>'
                </button>
                <button class="arrowRight arrows">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm2 12l-4.5 4.5 1.527 1.5 5.973-6-5.973-6-1.527 1.5 4.5 4.5z"/>
                    </svg>'
                </button>   
            `);

            let arrowLeft = this.slider.querySelector('.arrowLeft');
            let arrowRight = this.slider.querySelector('.arrowRight');
        
            arrowLeft.addEventListener('click', () => this.shiftLeft(this));
            arrowRight.addEventListener('click', (e) => this.shiftRight(e, this));
        }
    
        setListeners() {
            this.largeULs[0].addEventListener('click', (e) => this.zoomImage(e, this));
            this.modalWindow.addEventListener('click', (e) => this.zoomImage(e, this))
            this.largeULs[0].ondragstart = () => {
                return false;
            }
            this.minorULs[0].ondragstart = () => {
                return false;
            }
            this.minorULs[0].addEventListener('click', (e) => {
                if (e.target.hasAttribute('data-count')) {
                    clearTimeout(this.timer);
                    
                    this.largeLIs.forEach((item) => {
                        if (item.classList.contains('active')) {
                            item.classList.remove('active');
                        }
                    });
        
                    if (this.bigRes) {
                        this.pos = Number(e.target.getAttribute('data-count'));
                        this.largeLIs[this.pos].classList.add('active');
        
                        if (this.pos === this.largeLIs.length - 1) {
                            this.largeLIs[0].classList.add('active');
                            this.largeLIs[1].classList.add('active');
                        } else if (this.pos === this.largeLIs.length - 2) {
                            this.largeLIs[this.largeLIs.length - 1].classList.add('active');
                            this.largeLIs[0].classList.add('active');
                        } else {
                            this.largeLIs[this.pos+1].classList.add('active');
                            this.largeLIs[this.pos+2].classList.add('active');
                        }
        
                        this.setPosition(this.pos, this.pos+1, this.pos+2, this.pos, true)
                        this.dotsActiveSlide(this.pos, null, this.pos);
                    } else {
                        let newPos = Number(e.target.getAttribute('data-count'));
                        this.largeLIs[newPos].classList.add('active');
                        this.dotsActiveSlide(this.pos, null, newPos);
                        this.pos = newPos;
                    }
            
                    this.shiftElems();
                }
            });
        }
    
        setPosition(zeroIndex = 0, firstIndex = 1, secondIndex = 2, posIndex, direct) {
            const reCalc = () => {
                zeroIndex = this.largeLIs.length - 2;
                firstIndex = this.largeLIs.length - 1;
                secondIndex = 0;
            };

            const reSet = () => {
                zeroIndex = this.largeLIs.length - 1;
                firstIndex = 0;
                secondIndex = 1;
            };

            if (!direct) {
                if (this.left) {
                    if (posIndex === this.largeLIs.length) {
                        reSet();
                    } else if (posIndex === this.largeLIs.length - 1) {
                        reCalc();
                    }
                } else {
        
                    if (posIndex === this.largeLIs.length - 3) {
                        reCalc();
                    } else if (posIndex === this.largeLIs.length - 2) {
                        reSet();
                    } else if (posIndex === this.largeLIs.length - 1) {
                        zeroIndex = 0;
                        firstIndex = 1;
                        secondIndex = 2;
                    }
                }
        
            } else {
                if (posIndex === this.largeLIs.length - 2) {
                    reCalc();
                } else if (posIndex === this.largeLIs.length - 1) {
                    reSet();
                }
            }
        
            this.largeLIs[zeroIndex].style.left = '0px'
            this.largeLIs[firstIndex].style.left = `${this.elemWidth}px`;
            this.largeLIs[secondIndex].style.left = `${this.elemWidth*2}px`;
        }

        initSlider() {
            this.createMinorElems()
            this.preSets();
            this.shiftElems();
            this.setListeners();
            this.createArrows();
        }
    }
    
    const sliderObj = new Slider();
    
    sliderObj.initSlider();
});