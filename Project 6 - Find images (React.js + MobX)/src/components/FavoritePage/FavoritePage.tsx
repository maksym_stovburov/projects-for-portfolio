import React from 'react';
import { observer } from 'mobx-react-lite';
import { NavigateFunction, useNavigate } from 'react-router-dom';
import { IImageData } from '../../interfaces/interfaces';
import favoriteList from "../../store/favoriteList";
import iconLogoActive from "../../assets/heart-icon-active.svg";
import './FavoritePage.scss';

const FavoritePage: React.FC = observer(() => {
    const navigate: NavigateFunction = useNavigate();
    const goBack: number = -1;

    const addToFavoriteBtnClickHandler = (elem: IImageData): void => {
        elem.liked = !elem.liked;

        favoriteList.addNewElem(elem);
    };

    const goBackBtnClickHandler = (): void => {
        navigate(goBack);
    };

    return (
        <div className="main__favImages favImages">
            <button type="button" className="favImages__goBackBtn" onClick={goBackBtnClickHandler}>go back</button>
            <div className="favImages__outerContainer">
                {favoriteList.favoriteList.length ? 
                    <div className="favImages__outerContainer__listOfFavElems listOfFavElems">
                        {favoriteList.favoriteList.map((item: IImageData) => (
                            <div className="listOfFavElems__elemCell" key={item.id}>
                                <img src={`${item.webformatURL}`} alt="webformat"/>
                                <button type="button" data-value="liked" onClick={() => addToFavoriteBtnClickHandler(item)}>
                                    <img src={iconLogoActive} alt="heart-active"/>
                                </button>
                            </div>
                        ))}
                    </div> :
                    <div className="emptyPage">
                        <span>Nothing to show</span>
                    </div> 
                }
            </div>
        </div>
    );
});

export default FavoritePage;
