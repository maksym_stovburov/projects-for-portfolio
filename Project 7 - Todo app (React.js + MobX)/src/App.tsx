import React from 'react';
import { Routes, Route } from "react-router-dom";
import FinishedTodos from './components/FinishedTodos/FinishedTodos';
import Todos from './components/Todos/Todos';
import UnfinishedTodos from './components/UnfinishedTodos/UnfinishedTodos';
import './styles/style.scss';

const App: React.FC = () => {
  return (
    <Routes>
      <Route path="/" element={<Todos />}>
        <Route path="/open" element={<UnfinishedTodos />} />
        <Route path="/closed" element={<FinishedTodos />} />
        <Route path="*" element={<span>Something went wrong</span>} />
      </Route>
    </Routes>
  );
}

export default App;
