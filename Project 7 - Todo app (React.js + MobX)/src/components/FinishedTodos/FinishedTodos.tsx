import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { ITodo } from '../../interfaces/interfaces';
import todoList from '../../store/todoList';
import { Timer, TodosArr } from '../../types/types';
import BarTool from '../BarTool/BarTool';
import './FinishedTodos.scss';

const FinishedTodos: React.FC = observer(() => {
    const finishedTodos: TodosArr = todoList.toDoList.filter((item: ITodo) => item.finished === true);
    let timer: Timer;

    useEffect(() => {
        return(() => {
            clearTimeout(timer);
        })
    })

    const handleItemRemove = (id: number): void => {
        todoList.removeTodo(id);
    }

    const handleItemFinished = (id: number): void=> {
        timer = setTimeout(() => {
            todoList.setDone(id);
        }, 100);
    }

    return (
        <div className="main__finishedTodos todosGeneral">
            {finishedTodos.length ? 
                <ul>
                    {finishedTodos.map((item: ITodo) => (
                        <li className="main__finishedTodos__listItems" key={item.id}>
                            <button type="button" className="deleteBtn" onClick={() => handleItemRemove(item.id)}>delete</button>
                            <span>
                                {item.value}
                            </span>
                            <input type="checkbox" name="checkdone" id="checkdone" title="Mark as not done" checked onChange={() => handleItemFinished(item.id)}/>
                        </li>
                    ))}
                </ul> :
                <div className="todosGeneral__empty">
                    <h3>There is no closed tasks yet</h3>
                </div>
            }
            <BarTool />
        </div>
    );
});

export default FinishedTodos;
