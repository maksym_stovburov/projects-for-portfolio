import { DataArr } from '../../types/types'

const data: DataArr = [
    {
        name: "Sunday",
        ['Closed tasks']: 0,
    },
    {
        name: "Monday",
        ['Closed tasks']: 0,
    },
    {
        name: "Tuesday",
        ['Closed tasks']: 0,
    },
    {
        name: "Wednesday",
        ['Closed tasks']: 0,
    },
    {
        name: "Thursday",
        ['Closed tasks']: 0,
    },
    {
        name: "Friday",
        ['Closed tasks']: 0,
    },
    {
        name: "Saturday",
        ['Closed tasks']: 0,
    }
    ];

export { data };