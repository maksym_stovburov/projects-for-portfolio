import React, { useState } from 'react';
import { observer } from 'mobx-react-lite';
import toDoList from '../../store/todoList';
import { INewTodoProps, ITodo } from '../../interfaces/interfaces';
import './AddNewTodo.scss';

const AddNewTodo: React.FC<INewTodoProps> = observer((props) => {
    const [value, setValue] = useState<string>("");

    const handleInputValue = (e: React.ChangeEvent<HTMLInputElement>): void => {
        setValue(e.target.value);
    };

    const handleFormSubmit = (e: React.FormEvent): void => {
        e.preventDefault();

        if (value === "") {
            return;
        } else {
            const newTodo: ITodo = {id: Math.random() * 100, value: value, finished: false, existDate: new Date()};
            toDoList.addNewTodo(newTodo);
        }
    };

    const resetInput = (): void => {
        setValue("");
    };

    return (
        <div className="addTodoContainer__addNewTodo">
            <form onSubmit={handleFormSubmit} onReset={resetInput} className="addTodoContainer__addNewTodo__form form">
                <button type="button" className="form__closeBtn" onClick={() => props.setActive(false)}>close</button>
                <label htmlFor="desc">Write new todo</label>
                <input type="text" name="desc" id="desc" value={value} onChange={handleInputValue}/>
                <div className="form__buttonsContainer">
                    <button type="reset">reset</button>
                    <button type="submit">confirm</button>
                </div>
            </form>
        </div>
    );
});

export default AddNewTodo;
