import { makeAutoObservable } from "mobx";
import { IIncomingDataElem, ITodo } from "../interfaces/interfaces";
import { IncomingDataArr, TodosArr } from "../types/types";

class ToDoList {
    toDoList: TodosArr = [];

    constructor() {
        makeAutoObservable(this);
    }

    addNewTodo(todo: ITodo): void {
        this.toDoList.push(todo);
    }

    updateStoreOnload(todos: TodosArr): void {
        if (!todos) {
            return;
        }
        
        this.toDoList.push(...todos);
    }

    removeTodo(id: number): void {
        this.toDoList = this.toDoList.filter((item: ITodo) => item.id !== id)
    }

    setDone(id: number): void {
        const elem: ITodo = this.toDoList.find((item: ITodo) => item.id === id)!;
        elem.finished = !elem.finished;
    }

    clearStore(): void {
        this.toDoList.length = 0;
    }

    getExternalData(): void {
        const filterElem = (data: IncomingDataArr) => {
            data.forEach((item: IIncomingDataElem) => {
                const newTodo: ITodo = {id: Math.random() * 100, value: item.title, finished: false, existDate: new Date()};
                this.toDoList.push(newTodo);
            });
        };

        fetch('https://jsonplaceholder.typicode.com/posts')
            .then((response) => response.json())
            .then((json) => filterElem(json));
    }
}

export default new ToDoList();