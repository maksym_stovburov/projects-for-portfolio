import { IDataElem, IIncomingDataElem, ITodo } from "../interfaces/interfaces";

type TodosArr = Array<ITodo>;
type DataArr = Array<IDataElem>;
type IncomingDataArr = Array<IIncomingDataElem>;
type StateFinished = boolean | null;
type Timer = ReturnType<typeof setTimeout>;

export type { TodosArr, DataArr, IncomingDataArr, StateFinished, Timer };