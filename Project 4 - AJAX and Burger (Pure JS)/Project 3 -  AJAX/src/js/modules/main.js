"use strict";

const body = document.body;
const main = body.querySelector('main');
const modal = main.querySelector('.modal');
const modalMain = modal.querySelector('.modal__main');
let isWorking = false;
let removeButtons = document.getElementsByClassName('remove-button');

const addUsers = (users) => {
    let ul = document.createElement('ul');
    ul.classList.add('userList');

    for (let i = 0; i < 3; i++) {
        let li = document.createElement('li');

        li.dataset.id = `${users[users.length-(i+1)].id}`
        li.innerHTML = `
            <span>Name:</span> ${users[users.length-(i+1)].name}<br>
            <span>Username:</span> ${users[users.length-(i+1)].username}<br>
            <span>Email:</span> ${users[users.length-(i+1)].email}<br>
            <span>Address:</span> ${users[users.length-(i+1)].address?.suite} ${users[users.length-(i+1)].address?.street}<br>
            ${users[users.length-(i+1)].address?.city}, ${users[users.length-(i+1)].address?.zipcode}<br>
            <span>Phone:</span> ${users[users.length-(i+1)].phone}<br>
            <span>Website:</span> ${users[users.length-(i+1)].website}<br>
            <span>Company:</span> ${users[users.length-(i+1)].company?.name}<br>
        `;

        ul.append(li);
    }

    main.prepend(ul)
};

const setListeners = () => {
    let ul = document.getElementsByTagName('ul')[0];
    ul.addEventListener('click', (e) => showModal(Number(e.target.dataset.id)))
};

const createUserInfo = (total, id) => {
    let currentUser = total.filter((item) => item.userId == id);

        for (let i = 0; i < currentUser.length; i++) {
            let li = document.createElement('li');

            li.dataset.postId = currentUser[i].id;
            li.classList.add('li-modal');
            li.innerHTML = `
                <button class="remove-button"><svg viewBox="0 0 50 50"><path d="M 9.15625 6.3125 L 6.3125 9.15625 L 22.15625 25 L 6.21875 40.96875 L 9.03125 43.78125 L 25 27.84375 L 40.9375 43.78125 L 43.78125 40.9375 L 27.84375 25 L 43.6875 9.15625 L 40.84375 6.3125 L 25 22.15625 Z"></path></svg></button>
                <h2 class="modal__h2">${currentUser[i].title}</h2><br>
                <p>${currentUser[i].body}</p><br>
            `; 

            modalMain.append(li);
        }

        body.classList.add('overflowHdn');
        modal.classList.remove('hidden');

        for (let i = 0; i < removeButtons.length; i++) {
            removeButtons[i].addEventListener('click', (e) => deletePost(e.currentTarget, removeButtons));
        }

        modal.addEventListener("click", (e) => {
            if (e.target.classList.contains("modal__close") || e.target.classList.contains("modal")) {
                closeModal();
            } else if (e.target.classList.contains("modal__addNew")) {
                addNewPost(id);
            }
        });
};

const showModal = (id) => {
    fetch("https://jsonplaceholder.typicode.com/posts")
    .then(response => response.json())
    .then(total => createUserInfo(total, id))
    .catch((err) => console.log(`Something went wrong: ${err}`));
};

const addNewPost = (id) => {
    if (isWorking) {
        return;
    }

    let li = document.createElement('li');
    
    li.classList.add('modal-form');
    li.innerHTML = `
        <form class="form">
            <fieldset class="form__fieldset">
                <div><label for="header">Header:</label><input type="text" name="header" id="header" required></div>
                <div><label for="post">Post:</label><textarea id="post" name="body" cols="30" rows="5" required></textarea></div>
                <input type="submit" value="CONFIRM">
            </fieldset>
        </form>
    `;

    modalMain.prepend(li);

    let form = document.getElementsByClassName('form')[0];

    form && form.addEventListener('submit', (e) => {
        e.preventDefault();
        isWorking = false;

        let header = form.elements.header.value;
        let body = form.elements.body.value;

        li.classList.add('li-modal');
        li.innerHTML = `
            <button class="remove-button"><svg viewBox="0 0 50 50"><path d="M 9.15625 6.3125 L 6.3125 9.15625 L 22.15625 25 L 6.21875 40.96875 L 9.03125 43.78125 L 25 27.84375 L 40.9375 43.78125 L 43.78125 40.9375 L 27.84375 25 L 43.6875 9.15625 L 40.84375 6.3125 L 25 22.15625 Z"></path></svg></button>
            <h2 class="modal__h2">${header}</h2><br>
            <p>${body}</p><br>
        `;
        li.children[0].addEventListener('click', (e) => deletePost(e.currentTarget));

        fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body: JSON.stringify({
                title: `${header}`,
                body: `${body}`,
                userId: `${id}`,
        }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then((response) => console.log(`Resonse result: ${response.status}`))
        .catch((err) => console.log(`Something went wrong: ${err}`));
    });

    isWorking = true;
};

const deletePost = (target) => {
    let postId = Number(target.parentNode.dataset.postId);

    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
        method: 'DELETE',
    })
    .then ((response) => console.log(`Resonse result: ${response.status}`))
    .then (() => target.parentNode.remove())
    .then (() => {
        if (removeButtons.length === 0) {
            closeModal();
        }
    })
    .catch((err) => console.log(`Something went wrong: ${err}`));
};

const closeModal = () => {
    if (isWorking) {
        isWorking = false;
    }

    body.classList.remove('overflowHdn');
    modal.classList.add('hidden');
    modalMain.innerHTML = "";
};

const getUsers = () => {
    fetch("https://jsonplaceholder.typicode.com/users")
    .then(response => response.json())
    .then(total => addUsers(total))
    .then(() => setListeners())
    .catch((err) => console.log(`Something went wrong: ${err}`));
};

getUsers();