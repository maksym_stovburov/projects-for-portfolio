"use strict";

const initBurger = function(slide, fromTop) {
    const burgerBtn = document.querySelector('.burgerBtn');
    const burgerMenu = document.querySelector('.header__burgerMenu');
    const ul = burgerMenu.querySelector('.header__burgerMenu > ul');
    const main = document.querySelector('main');
    const html = document.documentElement;
    let isWorking = false;

    const showBurger = function(slide, fromTop) {
            html.classList.toggle('scrollOff');
            burgerMenu.classList.toggle('scrollOn');
            burgerBtn.classList.toggle('active');
            
            if (fromTop) {
                burgerMenu.classList.toggle('showBurger-top');
            } else {
                burgerMenu.classList.toggle('showBurger-left');
                main.classList.toggle('menu-active')
            }

            if (!slide && fromTop) {
                burgerMenu.classList.toggle('transitionFadeTop');
            } else if (!slide && !fromTop) {
                burgerMenu.classList.toggle('transitionFadeLeft');
            }

            if (isWorking) {
                ul.classList.toggle('visible');

                setTimeout(() => {
                    ul.classList.toggle('show');
                }, 500);
            } else {
                ul.classList.toggle('show');

                setTimeout(() => {
                    ul.classList.toggle('visible');
                }, 500);
            }

            isWorking = !isWorking;
        };

    const setBehaviour = function(slide, fromTop) {
        if (slide && fromTop) {
            burgerMenu.classList.add('header__burgerMenu-slide', 'header__burgerMenu-fromTop');
        } else if (slide && !fromTop) {
            burgerMenu.classList.add('header__burgerMenu-slide', 'header__burgerMenu-fromLeft');
        } else if (!slide && fromTop) {
            burgerMenu.classList.add('header__burgerMenu-fade', 'transitionFadeTop', 'header__burgerMenu-fromTop');
        } else if (!slide && !fromTop) {
            burgerMenu.classList.add('header__burgerMenu-fade', 'transitionFadeLeft', 'header__burgerMenu-fromLeft');
        } 
    };

    setBehaviour(slide, fromTop);

    burgerBtn.addEventListener('click', () => showBurger(slide, fromTop));

    const activateResizeHandler = function() {
        let flag = null;
        let timer = null;

        const removeClassHandler = function() {
            flag = false;
            html.classList.remove('resize-active');
        };

        const resizeHandler = function() {
            if (!flag) {
                flag = true;
                html.classList.add('resize-active');
            }

            if (isWorking) {
                showBurger(slide, fromTop);
            }

            clearTimeout(timer);
            timer = setTimeout(removeClassHandler, 100);
        };

        window.addEventListener('resize', resizeHandler);
        window.addEventListener('orientationchange', resizeHandler);
    };

    activateResizeHandler();
};

initBurger(false, false); /*To change burger's behaviour toggle function arguments*/